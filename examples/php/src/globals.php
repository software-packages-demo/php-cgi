<?php
declare(strict_types=1);

echo '$_GET:' . PHP_EOL;
var_dump($_GET);
echo '$_POST:' . PHP_EOL;
var_dump($_POST);
echo '$_FILES:' . PHP_EOL;
var_dump($_FILES);
echo '$_COOKIE:' . PHP_EOL;
var_dump($_COOKIE);
echo '$_SERVER:' . PHP_EOL;
var_dump($_SERVER);
echo '$_SESSION:' . PHP_EOL;
var_dump($_SESSION);
