<?php
declare(strict_types=1);

require '../vendor/autoload.php';


use Laminas\Diactoros\Response;
use Laminas\Diactoros\Response\TextResponse;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Laminas\HttpHandlerRunner\RequestHandlerRunner;
use Laminas\Stratigility\Middleware\ErrorResponseGenerator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
// use Throwable;


(new RequestHandlerRunner(
    new /** @psalm-immutable */ class
        implements
            RequestHandlerInterface {

        public function handle(ServerRequestInterface $request): ResponseInterface
        {
            return new TextResponse('Hello world!');
        }
    },
    new SapiEmitter,
    [ServerRequestFactory::class, 'fromGlobals'],
    function (Throwable $e) {
        return (new ErrorResponseGenerator())($e, new ServerRequest(), new Response());
    }
))->run();
